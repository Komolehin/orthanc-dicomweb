#!/bin/bash
set -ex

npm install --save-dev bootstrap-vue
mv node_modules/bootstrap-vue/dist/bootstrap-vue.css .
mv node_modules/bootstrap-vue/dist/bootstrap-vue.css.map .
mv node_modules/bootstrap-vue/dist/bootstrap-vue.js .
mv node_modules/bootstrap-vue/dist/bootstrap-vue.js.map .
mv node_modules/bootstrap-vue/dist/bootstrap-vue.min.css .
mv node_modules/bootstrap-vue/dist/bootstrap-vue.min.css.map .
mv node_modules/bootstrap-vue/dist/bootstrap-vue.min.js .
mv node_modules/bootstrap-vue/dist/bootstrap-vue.min.js.map .
rm -rf node_modules package-lock.json

set(JAVASCRIPT_LIBS_DIR  ${CMAKE_CURRENT_BINARY_DIR}/javascript-libs)
file(MAKE_DIRECTORY ${JAVASCRIPT_LIBS_DIR})

# We take "axios", "bootstrap" and "fontawesome" from the official
# Debian packages. We ship our own version of "babel polyfill" (not
# packaged yet), "bootstrap-vue" (not packaged yet), and "vue" (as
# bootstrap-vue requires version 2.6+, which is not packaged yet in
# Debian).

file(COPY
  ${CMAKE_SOURCE_DIR}/debian/ThirdPartyDownloads/babel-polyfill/polyfill.min.js
  ${CMAKE_SOURCE_DIR}/debian/ThirdPartyDownloads/bootstrap-vue/bootstrap-vue.min.js
  ${CMAKE_SOURCE_DIR}/debian/ThirdPartyDownloads/bootstrap-vue/bootstrap-vue.min.js.map
  ${CMAKE_SOURCE_DIR}/debian/ThirdPartyDownloads/vue/vue.min.js
  /usr/share/nodejs/bootstrap/dist/js/bootstrap.min.js
  DESTINATION
  ${JAVASCRIPT_LIBS_DIR}/js
  )

file(COPY_FILE
  /usr/share/nodejs/axios/dist/axios.min.js
  ${JAVASCRIPT_LIBS_DIR}/js/axios.min.js
  )

file(COPY_FILE
  /usr/share/nodejs/axios/dist/axios.min.js.map
  ${JAVASCRIPT_LIBS_DIR}/js/axios.min.map
  )

file(COPY
  ${CMAKE_SOURCE_DIR}/debian/ThirdPartyDownloads/bootstrap-vue/bootstrap-vue.min.css
  ${CMAKE_SOURCE_DIR}/debian/ThirdPartyDownloads/bootstrap-vue/bootstrap-vue.min.css.map
  /usr/share/fonts-font-awesome/css/font-awesome.min.css
  /usr/share/nodejs/bootstrap/dist/css/bootstrap.min.css
  /usr/share/nodejs/bootstrap/dist/css/bootstrap.min.css.map
  DESTINATION
  ${JAVASCRIPT_LIBS_DIR}/css
  )

file(COPY
  /usr/share/fonts-font-awesome/fonts/fontawesome-webfont.eot
  /usr/share/fonts-font-awesome/fonts/fontawesome-webfont.svg
  /usr/share/fonts-font-awesome/fonts/fontawesome-webfont.woff
  /usr/share/fonts-font-awesome/fonts/fontawesome-webfont.woff2
  /usr/share/fonts/opentype/font-awesome/FontAwesome.otf
  /usr/share/fonts/truetype/font-awesome/fontawesome-webfont.ttf
  DESTINATION
  ${JAVASCRIPT_LIBS_DIR}/fonts
  )

file(COPY
  ${CMAKE_SOURCE_DIR}/Resources/Orthanc/OrthancLogo.png
  DESTINATION
  ${JAVASCRIPT_LIBS_DIR}/img
  )

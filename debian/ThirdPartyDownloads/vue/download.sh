#!/bin/bash
set -ex

npm install --save-dev vue
mv node_modules/vue/dist/vue.js .
mv node_modules/vue/dist/vue.min.js .
rm -rf node_modules package-lock.json
